import { Component, Input } from '@angular/core';
import { Offer } from '../../model/offer/offer';
import { OfferType } from '../../model/offer/offer-type';

@Component({
  selector: 'app-offer-list-item[offer]',
  templateUrl: './offer-list-item.component.html',
  styleUrls: ['./offer-list-item.component.scss']
})
export class OfferListItemComponent {
  @Input() public offer!: Offer;

  public readonly directionIconsMap: Record<OfferType, string> = {
    [OfferType.BUY]: 'assets/offer-direction-buy.svg',
    [OfferType.SELL]: 'assets/offer-direction-sell.svg'
  };

  constructor() {}

  public get isBuy() {
    return this.offer.type === OfferType.BUY;
  }

  public get isSell() {
    return this.offer.type === OfferType.SELL;
  }
}
