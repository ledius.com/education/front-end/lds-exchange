import { Component, Input, OnInit } from '@angular/core';
import { OfferService } from '../../services/offer.service';
import { Offer } from '../../model/offer/offer';
import { OfferType } from '../../model/offer/offer-type';

@Component({
  selector: 'app-offer-list',
  templateUrl: './offer-list.component.html',
  styleUrls: ['./offer-list.component.scss']
})
export class OfferListComponent implements OnInit {
  public offers: Offer[] = [];
  @Input() public types: OfferType[] = [OfferType.BUY, OfferType.SELL];
  @Input() public title!: string;
  public action: string = '';

  constructor(protected readonly offerService: OfferService) {
    this.offerService.offers$.subscribe((offers) => {
      this.offers = offers.filter((offer) => this.types.includes(offer.type));
    });
  }

  async ngOnInit(): Promise<void> {
    await this.offerService.fetchOffers();
  }
}
