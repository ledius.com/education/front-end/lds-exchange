import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OfferListItemComponent } from './offer-list-item/offer-list-item.component';
import { OfferListComponent } from './offer-list/offer-list.component';
import {
  IgxAvatarModule,
  IgxButtonModule,
  IgxDividerModule,
  IgxLayoutModule,
  IgxRippleModule
} from 'igniteui-angular';

@NgModule({
  declarations: [OfferListItemComponent, OfferListComponent],
  imports: [
    CommonModule,
    IgxLayoutModule,
    IgxAvatarModule,
    IgxButtonModule,
    IgxDividerModule,
    IgxRippleModule
  ],
  exports: [OfferListComponent, OfferListItemComponent]
})
export class OfferModule {}
