import { Component, OnInit } from '@angular/core';
import { DrawerService } from '../services/drawer.service';
import { IgxIconService } from 'igniteui-angular';

@Component({
  selector: 'app-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.scss']
})
export class DrawerComponent {
  public opened: boolean = false;

  public readonly links: { path: string; icon: string; title: string }[] = [
    { title: 'Swap', icon: 'swap', path: '/exchange' },
    { title: 'Lending', icon: 'deposit', path: '/lending' },
    { title: 'Accounts', icon: 'accounts', path: '/bank-details' },
    { title: 'History', icon: 'history', path: '/history' }
  ];

  constructor(
    private readonly drawerService: DrawerService,
    private readonly igxIconService: IgxIconService
  ) {
    this.drawerService.opened$.subscribe((value) => (this.opened = value));
  }

  public open(): void {
    this.drawerService.open();
  }

  public close(): void {
    this.drawerService.close();
  }
}
