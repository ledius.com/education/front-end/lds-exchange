import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrawerComponent } from './drawer.component';
import { RouterModule } from '@angular/router';
import {
  IgxDividerModule,
  IgxIconModule,
  IgxLayoutModule,
  IgxNavigationDrawerModule
} from 'igniteui-angular';

@NgModule({
  declarations: [DrawerComponent],
  exports: [DrawerComponent],
  imports: [
    CommonModule,
    RouterModule,
    IgxNavigationDrawerModule,
    IgxIconModule,
    IgxLayoutModule,
    IgxDividerModule
  ]
})
export class DrawerModule {}
