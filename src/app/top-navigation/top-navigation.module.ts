import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopNavigationComponent } from './top-navigation.component';
import {
  IgxButtonModule,
  IgxDropDownModule,
  IgxIconModule,
  IgxNavbarModule,
  IgxNavigationDrawerModule,
  IgxToggleModule
} from 'igniteui-angular';

@NgModule({
  declarations: [TopNavigationComponent],
  imports: [
    CommonModule,
    IgxNavbarModule,
    IgxIconModule,
    IgxToggleModule,
    IgxDropDownModule,
    IgxNavigationDrawerModule,
    IgxButtonModule
  ],
  bootstrap: [TopNavigationComponent],
  exports: [TopNavigationComponent]
})
export class TopNavigationModule {}
