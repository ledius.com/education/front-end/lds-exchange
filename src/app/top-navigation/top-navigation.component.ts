import { Component, OnInit } from '@angular/core';
import { DrawerService } from '../services/drawer.service';
import {
  ConnectedPositioningStrategy,
  HorizontalAlignment,
  NoOpScrollStrategy,
  OverlaySettings,
  VerticalAlignment
} from 'igniteui-angular';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-top-navigation',
  templateUrl: './top-navigation.component.html',
  styleUrls: ['./top-navigation.component.scss']
})
export class TopNavigationComponent {
  public readonly dropdownOverlaySettings: OverlaySettings = {
    positionStrategy: new ConnectedPositioningStrategy({
      horizontalDirection: HorizontalAlignment.Left,
      horizontalStartPoint: HorizontalAlignment.Right,
      verticalStartPoint: VerticalAlignment.Bottom
    }),
    scrollStrategy: new NoOpScrollStrategy()
  };

  public isAuth: boolean = false;

  constructor(
    private readonly drawerService: DrawerService,
    private readonly authService: AuthService
  ) {
    this.authService.isAuth$.subscribe((value) => (this.isAuth = value));
  }

  public async googleSignIn(): Promise<void> {
    await this.authService.googleSignIn();
  }

  public toggleDrawer(): void {
    this.drawerService.toggle();
  }

  public async logout(): Promise<void> {
    await this.authService.logout();
  }
}
