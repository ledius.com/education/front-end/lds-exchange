import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BankDetailsComponent } from './bank-details.component';



@NgModule({
  declarations: [
    BankDetailsComponent
  ],
  imports: [
    CommonModule
  ]
})
export class BankDetailsModule { }
