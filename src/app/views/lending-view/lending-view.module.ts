import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LendingViewComponent } from './lending-view/lending-view.component';



@NgModule({
  declarations: [
    LendingViewComponent
  ],
  imports: [
    CommonModule
  ]
})
export class LendingViewModule { }
