import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExchangeComponent } from './exchange.component';
import { BalanceModule } from '../../balance/balance.module';
import { IgxLayoutModule } from 'igniteui-angular';
import { OfferModule } from '../../offer/offer.module';
import { BaseViewModule } from '../base-view/base-view.module';

@NgModule({
  declarations: [ExchangeComponent],
  imports: [
    CommonModule,
    BalanceModule,
    IgxLayoutModule,
    OfferModule,
    BaseViewModule
  ]
})
export class ExchangeModule {}
