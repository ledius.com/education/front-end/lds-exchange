import { Component } from '@angular/core';
import { OfferType } from '../../model/offer/offer-type';

@Component({
  selector: 'app-exchange',
  templateUrl: './exchange.component.html',
  styleUrls: ['./exchange.component.scss']
})
export class ExchangeComponent {
  public offerType: typeof OfferType = OfferType;

  constructor() {}
}
