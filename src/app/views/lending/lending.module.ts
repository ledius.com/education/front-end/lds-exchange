import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LendingComponent } from './lending.component';



@NgModule({
  declarations: [
    LendingComponent
  ],
  imports: [
    CommonModule
  ]
})
export class LendingModule { }
