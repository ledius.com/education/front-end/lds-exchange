import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExchangeModule } from '../exchange/exchange.module';
import { HistoryModule } from '../history/history.module';
import { LendingModule } from '../lending/lending.module';
import { BankDetailsModule } from '../bank-details/bank-details.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ExchangeModule,
    HistoryModule,
    LendingModule,
    BankDetailsModule
  ]
})
export class ViewsModule {}
