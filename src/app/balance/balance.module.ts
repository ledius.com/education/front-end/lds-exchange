import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BalanceComponent } from './balance.component';
import {
  IgxAvatarModule,
  IgxButtonModule,
  IgxDividerModule,
  IgxLayoutModule,
  IgxRippleModule
} from 'igniteui-angular';

@NgModule({
  declarations: [BalanceComponent],
  exports: [BalanceComponent],
  imports: [
    CommonModule,
    IgxLayoutModule,
    IgxAvatarModule,
    IgxDividerModule,
    IgxButtonModule,
    IgxRippleModule
  ]
})
export class BalanceModule {}
