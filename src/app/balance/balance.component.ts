import { Component, OnInit } from '@angular/core';
import { Balance } from '../model/balance/balance';
import { BalanceService } from '../services/balance.service';
import { AuthService } from '../services/auth.service';
import { AccountService } from '../services/account.service';
import { SymbolEnum } from '../model/asset/symbol-enum';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.scss']
})
export class BalanceComponent implements OnInit {
  public balances: Balance[] = [];

  public get LDSBalance() {
    return (
      this.balances.find((item) => item.symbol === SymbolEnum.LDS) ||
      new Balance(BigInt(0), SymbolEnum.LDS)
    );
  }

  constructor(
    protected readonly balanceService: BalanceService,
    protected readonly authService: AuthService,
    protected readonly accountService: AccountService
  ) {}

  public async ngOnInit(): Promise<void> {
    this.authService.authInfo$.subscribe(async (value) => {
      if (!value) {
        return;
      }
      const account = await this.accountService.getAccount(value.id);
      this.balances = await this.balanceService.balancesOf(account.address);
    });
  }
}
