import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',

  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'ledius-exchange';
  public isAuth: boolean = false;

  constructor(private readonly authService: AuthService) {
    this.authService.isAuth$.subscribe((isAuth) => (this.isAuth = isAuth));
  }
}
