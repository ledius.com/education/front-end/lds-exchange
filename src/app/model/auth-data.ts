export class AuthData {
  public readonly accessToken: string;
  public readonly refreshToken: string;
  public readonly type: 'bearer';
  public readonly expiresIn: Date;

  constructor(data: AuthData) {
    this.accessToken = data.accessToken;
    this.refreshToken = data.refreshToken;
    this.type = data.type;
    this.expiresIn = data.expiresIn;
  }
}
