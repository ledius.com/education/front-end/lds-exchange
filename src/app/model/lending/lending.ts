import { Account } from '../account/account';
import { Transform, Type } from 'class-transformer';
import { LendingStatusEnum } from './lending-status-enum';

export class Lending {
  public readonly id!: string;

  @Type(() => Account)
  public readonly account!: Account;

  @Transform((value) => BigInt(value.value))
  public readonly amount!: bigint;

  @Transform((value) => BigInt(value.value))
  public readonly interestsAmount!: bigint;

  @Type(() => Date)
  public readonly lastInterestsAt!: Date;

  public readonly status!: LendingStatusEnum;

  @Type(() => Date)
  public readonly endDate!: Date;

  @Type(() => Date)
  public readonly createdAt!: Date;
}
