export interface Rate {
  readonly currency: string;
  readonly price: number;
}
