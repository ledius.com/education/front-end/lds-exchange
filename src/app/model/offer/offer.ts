import { Rate } from './rate';
import { OfferStatus } from './offer-status';
import { OfferType } from './offer-type';
import { Account } from '../account/account';
import { BankDetails } from '../bank-details/bank-details';
import { Type } from 'class-transformer';

export class Offer {
  public readonly id!: string;
  public readonly amount!: bigint;
  public readonly rate!: Rate;
  public readonly status!: OfferStatus;
  public readonly type!: OfferType;

  @Type(() => Account)
  public readonly owner!: Account;

  @Type(() => Account)
  public readonly reservedBy!: Account;

  @Type(() => Date)
  public readonly reservedAt!: Date;

  @Type(() => BankDetails)
  public readonly bankDetails?: BankDetails;

  @Type(() => Date)
  public readonly publishedAt!: Date;

  @Type(() => Date)
  public readonly createdAt!: Date;

  @Type(() => Date)
  public readonly updatedAt!: Date;
}
