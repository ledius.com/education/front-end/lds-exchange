import { OfferType } from './offer-type';
import { OfferStatus } from './offer-status';

export class OfferListRequest {
  public readonly type?: OfferType;
  public readonly status?: OfferStatus;
}
