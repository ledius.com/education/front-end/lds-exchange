import { Transform } from 'class-transformer';
import { SymbolEnum } from '../asset/symbol-enum';

export class Balance {
  @Transform((value) => BigInt(value.value))
  public readonly balance: bigint;

  public readonly symbol: SymbolEnum;

  public get decimals(): number {
    return 18;
  }

  constructor(balance: bigint, symbol: SymbolEnum) {
    this.balance = balance;
    this.symbol = symbol;
  }

  public toHumanize(): string {
    const [scale, precision] = [
      BigInt(this.balance) / BigInt(10 ** this.decimals),
      String(this.balance)
        .split('')
        .slice(-18)
        .join('')
        .padStart(this.decimals, '0')
    ];
    return `${scale}.${precision}`;
  }

  public toJSON() {
    return {
      balance: this.balance.toString(),
      symbol: this.symbol
    };
  }
}
