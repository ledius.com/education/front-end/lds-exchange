export class Pagination {
  public readonly order?: string;
  public readonly orderBy?: 'ASC' | 'DESC';
  public readonly page: number = 0;
  public readonly pageSize: number = 10;
  public readonly total: number = 0;
}
