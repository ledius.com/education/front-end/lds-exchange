import { Pagination } from './pagination';
import { Offer } from './offer/offer';

export interface PaginationResponse<T> {
  readonly pagination: Pagination;
  readonly items: T[];
}
