export class Account {
  public readonly id!: string;
  public readonly userId: string;
  public readonly address: string;

  constructor(userId: string, address: string) {
    this.userId = userId;
    this.address = address;
  }
}
