import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { AuthData } from '../model/auth-data';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthResolver implements Resolve<AuthData | null> {
  constructor(private readonly authService: AuthService) {}

  public async resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<AuthData | null> {
    console.log('auth resolver is running');
    return await firstValueFrom(this.authService.authData$.asObservable());
  }
}
