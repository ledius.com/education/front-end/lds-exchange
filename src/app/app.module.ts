import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TopNavigationModule } from './top-navigation/top-navigation.module';
import { DrawerModule } from './drawer/drawer.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { IgxIconModule } from 'igniteui-angular';
import { ViewsModule } from './views/views/views.module';
import { LocalIconService } from './services/local-icon.service';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import {
  GoogleLoginProvider,
  SocialAuthServiceConfig,
  SocialLoginModule
} from 'angularx-social-login';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [AppComponent],
  imports: [
    HttpClientModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    BrowserAnimationsModule,
    TopNavigationModule,
    DrawerModule,
    ViewsModule,
    IgxIconModule,
    SocialLoginModule
  ],
  bootstrap: [AppComponent],
  providers: [
    LocalIconService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: true,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(environment.googleAuthClientId)
          }
        ]
      } as SocialAuthServiceConfig
    }
  ]
})
export class AppModule {
  constructor(private readonly localIconService: LocalIconService) {}
}
