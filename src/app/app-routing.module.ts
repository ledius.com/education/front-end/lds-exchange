import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExchangeComponent } from './views/exchange/exchange.component';
import { LendingComponent } from './views/lending/lending.component';
import { BankDetailsComponent } from './views/bank-details/bank-details.component';
import { HistoryComponent } from './views/history/history.component';
import { AuthResolver } from './resolvers/auth.resolver';

const routes: Routes = [
  { path: '', redirectTo: 'exchange', pathMatch: 'full' },
  {
    path: 'exchange',
    component: ExchangeComponent,
    resolve: {
      authData: AuthResolver
    }
  },
  { path: 'lending', component: LendingComponent, canActivate: [] },
  {
    path: 'bank-details',
    component: BankDetailsComponent,
    canActivate: []
  },
  { path: 'history', component: HistoryComponent, canActivate: [] }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: 'enabledBlocking'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
