import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LendingLListItemComponent } from './lending-llist-item.component';

describe('LendingLListItemComponent', () => {
  let component: LendingLListItemComponent;
  let fixture: ComponentFixture<LendingLListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LendingLListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LendingLListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
