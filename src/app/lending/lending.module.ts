import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LendingLListItemComponent } from './lending-llist-item/lending-llist-item.component';
import { LendingListComponent } from './lending-list/lending-list.component';



@NgModule({
  declarations: [
    LendingLListItemComponent,
    LendingListComponent
  ],
  imports: [
    CommonModule
  ]
})
export class LendingModule { }
