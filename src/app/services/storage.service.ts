import { Injectable } from '@angular/core';
import { PlatformService } from './platform.service';

@Injectable({
  providedIn: 'root'
})
export class StorageService implements Storage {
  constructor(private readonly platformService: PlatformService) {}

  public readonly length = 0;

  public clear(): void {
    if (this.storageAvailable()) {
      window.localStorage.clear();
    }
  }

  public getItem(key: string): string | null {
    if (this.storageAvailable()) {
      return window.localStorage.getItem(key);
    }
    return null;
  }

  public key(index: number): string | null {
    if (this.storageAvailable()) {
      return window.localStorage.key(index);
    }
    return null;
  }

  public removeItem(key: string): void {
    if (this.storageAvailable()) {
      window.localStorage.removeItem(key);
    }
  }

  public setItem(key: string, value: string): void {
    if (this.storageAvailable()) {
      window.localStorage.setItem(key, value);
    }
  }

  public storageAvailable() {
    return this.platformService.isBrowser();
  }
}
