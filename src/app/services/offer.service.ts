import { Injectable } from '@angular/core';
import { BehaviorSubject, filter, lastValueFrom, mergeMap, of } from 'rxjs';
import { Offer } from '../model/offer/offer';
import { HttpClient } from '@angular/common/http';
import { PaginationResponse } from '../model/pagination-response';
import { Pagination } from '../model/pagination';
import { instanceToPlain, plainToInstance } from 'class-transformer';
import { OfferListRequest } from '../model/offer/offer-list-request';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class OfferService {
  public readonly pagination$: BehaviorSubject<Pagination> =
    new BehaviorSubject<Pagination>(new Pagination());

  public readonly offers$: BehaviorSubject<Offer[]> = new BehaviorSubject<
    Offer[]
  >([]);

  constructor(
    protected readonly http: HttpClient,
    private readonly authService: AuthService
  ) {}

  public async fetchOffers(
    request: OfferListRequest = new OfferListRequest()
  ): Promise<void> {
    await lastValueFrom(
      this.authService.makeAuthRequest().pipe(
        mergeMap(() =>
          this.http
            .get<PaginationResponse<Offer>>('/api/ledius-token/offers', {
              params: instanceToPlain(request)
            })
            .pipe(
              mergeMap(({ items, pagination }) => {
                this.offers$.next(plainToInstance(Offer, items));
                this.pagination$.next(plainToInstance(Pagination, pagination));

                return of(void 0);
              })
            )
        )
      )
    );
  }
}
