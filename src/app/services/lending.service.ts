import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Lending } from '../model/lending/lending';
import { lastValueFrom, map } from 'rxjs';
import { plainToInstance } from 'class-transformer';

@Injectable({
  providedIn: 'root'
})
export class LendingService {
  constructor(private readonly httpClient: HttpClient) {}

  public async create(
    userId: string,
    amount: bigint,
    endDate: Date
  ): Promise<Lending> {
    return await lastValueFrom(
      this.httpClient
        .post<Lending>('/api/ledius-token/lending', {
          userId,
          amount: String(amount),
          endDate
        })
        .pipe(map((lending) => plainToInstance(Lending, lending)))
    );
  }

  public async getList(userId: string): Promise<Lending[]> {
    return await lastValueFrom(
      this.httpClient
        .get<Lending[]>(`/api/ledius-token/lending/${userId}`)
        .pipe(map((lendingList) => plainToInstance(Lending, lendingList)))
    );
  }
}
