import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { Platform } from '@angular/cdk/platform';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class PlatformService {
  constructor(@Inject(PLATFORM_ID) private readonly platformId: Platform) {}

  public isBrowser(): boolean {
    return isPlatformBrowser(this.platformId);
  }
}
