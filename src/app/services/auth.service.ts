import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  BehaviorSubject,
  delayWhen,
  filter,
  from,
  lastValueFrom,
  mergeMap,
  Observable,
  of,
  tap
} from 'rxjs';
import { AuthData } from '../model/auth-data';
import { Router } from '@angular/router';
import { PlatformService } from './platform.service';
import { GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';
import { AuthInfo } from '../model/auth/auth-info';
import { plainToInstance } from 'class-transformer';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public readonly isAuth$: BehaviorSubject<boolean> =
    new BehaviorSubject<boolean>(false);
  public readonly authData$: BehaviorSubject<AuthData | null> =
    new BehaviorSubject<AuthData | null>(null);
  public readonly authInfo$: BehaviorSubject<AuthInfo | null> =
    new BehaviorSubject<AuthInfo | null>(null);

  constructor(
    protected readonly http: HttpClient,
    protected readonly platformService: PlatformService,
    protected readonly socialAuthService: SocialAuthService,
    protected readonly router: Router
  ) {
    this.socialAuthService.authState.subscribe(async (socialUser) => {
      await this.authByIdToken(socialUser?.idToken);
    });
    this.authData$.subscribe(async (value) => {
      if (value) {
        await this.authInfo();
      }
    });
    this.authInfo$.subscribe((info) => {
      this.isAuth$.next(Boolean(info));
    });
  }

  public async googleSignIn(): Promise<void> {
    if (this.platformService.isBrowser()) {
      await this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
    }
  }

  public async authByIdToken(idToken: string | null): Promise<void> {
    if (this.platformService.isBrowser() && idToken) {
      const plainData = await lastValueFrom(
        this.http.post<AuthData>('/api/v1/auth/google-sign-in', {
          idToken: idToken
        })
      );

      const authData = new AuthData(plainData);
      this.authData$.next(authData);
    }
  }

  public async authInfo(): Promise<void> {
    await lastValueFrom(
      this.authData$.asObservable().pipe(
        filter(Boolean),
        mergeMap(() => {
          return this.http.get<AuthInfo>('/api/v1/auth/info').pipe(
            mergeMap((authInfo) => {
              this.authInfo$.next(plainToInstance(AuthInfo, authInfo));
              return of(void 0);
            })
          );
        })
      )
    );
  }

  public getAuthorizationHeader(): { Authorization: string } {
    return {
      Authorization: `bearer ${this.authData$.getValue()?.accessToken}` ?? ''
    };
  }

  public async logout(): Promise<void> {
    if (this.platformService.isBrowser()) {
      this.authData$.next(null);
      await this.socialAuthService.signOut(true);
    }
  }

  public makeAuthRequest(): Observable<true> {
    return this.isAuth$.asObservable().pipe(filter(Boolean));
  }
}
