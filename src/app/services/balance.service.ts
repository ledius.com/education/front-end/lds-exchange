import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Balance } from '../model/balance/balance';
import { lastValueFrom, map } from 'rxjs';
import { plainToInstance } from 'class-transformer';

@Injectable({
  providedIn: 'root'
})
export class BalanceService {
  constructor(private readonly httpClient: HttpClient) {}

  public async balancesOf(address: string): Promise<Balance[]> {
    return lastValueFrom(
      this.httpClient
        .get<Balance[]>('/api/ledius-token/balance/account', {
          params: { address }
        })
        .pipe(map((balances) => plainToInstance(Balance, balances)))
    );
  }
}
