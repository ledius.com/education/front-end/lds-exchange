import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Account } from '../model/account/account';
import { lastValueFrom, map } from 'rxjs';
import { plainToInstance } from 'class-transformer';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  constructor(private readonly httpClient: HttpClient) {}

  public async create(userId: string): Promise<Account> {
    return lastValueFrom(
      this.httpClient
        .post<Account>('/api/ledius-token/accounts', {
          userId
        })
        .pipe(map((account) => plainToInstance(Account, account)))
    );
  }

  public async getAccounts(): Promise<Account[]> {
    return lastValueFrom(
      this.httpClient
        .get<Account[]>('/api/ledius-token/accounts')
        .pipe(map((accounts) => plainToInstance(Account, accounts)))
    );
  }

  public async getAccount(userId: string): Promise<Account> {
    return lastValueFrom(
      this.httpClient
        .get<Account>(`/api/ledius-token/accounts/${userId}/user`)
        .pipe(map((account) => plainToInstance(Account, account)))
    );
  }
}
