import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DrawerService {
  public readonly opened$: BehaviorSubject<boolean> =
    new BehaviorSubject<boolean>(false);

  public toggle() {
    this.opened$.next(!this.opened$.getValue());
  }

  public open() {
    this.opened$.next(true);
  }

  public close() {
    this.opened$.next(false);
  }
}
