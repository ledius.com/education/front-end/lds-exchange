import { Injectable } from '@angular/core';
import { PlatformService } from './platform.service';
import { IgxIconService } from 'igniteui-angular';

export interface SvgIcon {
  name: string;
  family: string;
  url: string;
}

@Injectable({
  providedIn: 'root'
})
export class LocalIconService {
  private readonly icons: { name: string; family: string; url: string }[] = [
    { name: 'accounts', url: '/assets/menu/accounts.svg', family: 'menu' },
    { name: 'deposit', url: '/assets/menu/deposit.svg', family: 'menu' },
    { name: 'history', url: '/assets/menu/history.svg', family: 'menu' },
    { name: 'swap', url: '/assets/menu/swap.svg', family: 'menu' }
  ];

  constructor(
    private readonly platformService: PlatformService,
    private readonly igxIconService: IgxIconService
  ) {
    this.addSvgIcons(...this.icons);
  }

  public addSvgIcons(...icons: SvgIcon[]): void {
    icons
      .filter(() => this.platformService.isBrowser())
      .forEach(({ name, url, family }) =>
        this.igxIconService.addSvgIcon(name, url, family)
      );
  }
}
